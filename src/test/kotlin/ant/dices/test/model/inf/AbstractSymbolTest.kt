package ant.dices.test.model.inf

import ant.dices.model.CriticalSymbol
import ant.dices.model.Dice
import ant.dices.model.Symbol
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.TestFactory

internal class AbstractSymbolTest {

    @Nested
    @DisplayName("equals")
    internal inner class Cequals {

        private val sameId  = Symbol("id", "other_opposite")
        private val opposite = Symbol("eq_id", "eq_opposite")
        private val origin = CriticalSymbol("id", "opposite", opposite)

        private val testArgs = mapOf(
                origin to true,
                sameId to true,
                opposite to false,
                Dice.BLANK to false,
                null to false)

        @TestFactory
        @DisplayName("should return false when comparing to NOT AbstractSymbol")
        fun equals_returns_givenOtherInstance() = testArgs.map {
            DynamicTest.dynamicTest("should return ${it.value} when given ${it.key}") {
                assertThat(origin == it.key).isEqualTo(it.value)
            }
        }
    }
}