package ant.dices.test.model

import ant.dices.model.Face
import ant.dices.model.Symbol
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class FaceTest {
    @Test
    @DisplayName("should return the symbols present on the face")
    fun get_symbols() {

        val face = Face(
                Symbol("success", "failure"),
                Symbol("failure", "success")
        )

        val result = face.opposables
        assertThat(result).containsExactlyInAnyOrder(Symbol("failure"),
                Symbol("success"))
    }

    @Test
    @DisplayName("should return empty list if no Symbol defined on the face")
    fun get_retunsEmpty() {
        val result = Face.BLANK.opposables
        assertThat(result).isEmpty()
    }
}
