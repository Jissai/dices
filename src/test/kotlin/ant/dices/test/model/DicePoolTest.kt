package ant.dices.test.model

import ant.dices.model.Dice
import ant.dices.model.DicePool
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class DicePoolTest {
    val dicePool = DicePool()

    @Nested
    @DisplayName("Add")
    inner class Add {

        @Test
        @DisplayName("should add a new dice to the pool")
        fun add_new_dice() {
            assertThat(dicePool.add(Dice.BLANK)).isEqualTo(1)

            val dices = dicePool.list()
            assertThat(dices.size).isEqualTo(1)
            assertThat(dices[Dice.BLANK]).isEqualTo(1)
        }

        @Test
        @DisplayName("should add 3 occurence of a dice to the pool")
        fun add_multiple_new_dice() {
            assertThat(dicePool.add(Dice.BLANK, 3)).isEqualTo(3)

            val dices = dicePool.list()
            assertThat(dices.size).isEqualTo(1)
            assertThat(dices[Dice.BLANK]).isEqualTo(3)
        }

        @Test
        @DisplayName("should add an occurrence of dice to the pool")
        fun add_existing_dice() {
            assertThat(dicePool.add(Dice.BLANK)).isEqualTo(1)
            assertThat(dicePool.add(Dice.BLANK)).isEqualTo(2)

            val dices = dicePool.list()
            assertThat(dices.size).isEqualTo(1)
            assertThat(dices[Dice.BLANK]).isEqualTo(2)
        }

        @Test
        @DisplayName("should add an occurrence of dice to the pool")
        fun add_multiple_existing_dice() {
            assertThat(dicePool.add(Dice.BLANK)).isEqualTo(1)
            assertThat(dicePool.add(Dice.BLANK, 2)).isEqualTo(3)

            val dices = dicePool.list()
            assertThat(dices.size).isEqualTo(1)
            assertThat(dices[Dice.BLANK]).isEqualTo(3)
        }
    }

    @Nested
    @DisplayName("Remove")
    inner class Remove {

        val dice = Dice("test")
        @BeforeEach
        fun setUp() {
            assertThat(dicePool.add(dice, 3)).isEqualTo(3)
        }

        @Test
        @DisplayName("should do nothing and return 0 when removing Dice not present in pool")
        fun remove_do_nothing_dice_not_present() {
            assertThat(dicePool.remove(Dice.BLANK)).isEqualTo(0)
        }

        @Test
        @DisplayName("should remove multiple occurrence of a dice")
        fun remove_occurrences() {
            assertThat(dicePool.remove(dice, 2)).isEqualTo(1)

            val dices = dicePool.list()
            assertThat(dices.size).isEqualTo(1)
            assertThat(dices[dice]).isEqualTo(1)
        }

        @Test
        @DisplayName("should remove the dice from the pool if no more occurrences")
        fun remove_dice() {
            assertThat(dicePool.remove(dice, 3)).isEqualTo(0)

            val dices = dicePool.list()
            assertThat(dices.size).isEqualTo(0)
            assertThat(dices[dice]).isEqualTo(null)
        }

        @Test
        @DisplayName("should remove the dice from the pool if no more occurrences")
        fun remove_too_many_occurrences() {
            assertThat(dicePool.remove(dice, 15)).isEqualTo(0)

            val dices = dicePool.list()
            assertThat(dices.size).isEqualTo(0)
            assertThat(dices[dice]).isEqualTo(null)
        }

    }
}
