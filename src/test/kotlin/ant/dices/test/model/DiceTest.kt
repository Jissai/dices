package ant.dices.test.model

import ant.dices.model.Dice
import ant.dices.model.Face
import ant.dices.model.Symbol
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatCode
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory

internal class DiceTest {

    private val ga = Symbol("ga")
    private val faceGa = Face(ga)

    @Nested
    @DisplayName("constructor")
    internal inner class TestConstructor {

        @Test
        @DisplayName("should not fail if constructor is given empty arrray")
        fun ctor_givenEmpty() {
            assertThatCode { Dice.BLANK }.doesNotThrowAnyException()
        }
    }

    @Nested
    @DisplayName("getNumberOfFaces")
    internal inner class GetNumberOfFaces {

        @Test
        @DisplayName("should return 0 given an empty Dice")
        fun getNumberOfFaces_returns0_givenEmptyDice() {
            assertThat(Dice.BLANK.faces.size).isEqualTo(0)
        }

        @Test
        @DisplayName("should return the correct numbr of faces given a Dice")
        fun getNumberOfFaces_returnsNumberOfFaces_givenDice() {
            assertThat(Dice("test", Face.BLANK, Face.BLANK).faces.size).isEqualTo(2)
        }
    }

    @Nested
    @DisplayName("getSymbolsOnFace")
    internal inner class GetSymbolsOnFace {
        private var failure = Symbol("failure", "success")
        private var menace = Symbol("menace", "avantage")
        private var face1 = Face(failure)
        private var face2 = Face(failure, menace)
        private var dice = Dice("test", Face.BLANK, face1, face2, Face.BLANK, face1, face2)

        @Test
        @DisplayName("should return expected list of symbols for an existing given Face")
        fun getSymbolsOfFaces_returns_symbols() {
            assertThat(dice.getSymbolsOnFace(2)).hasSize(2).containsExactlyInAnyOrder(*face2.opposables)
            assertThat(dice.getSymbolsOnFace(3)).hasSize(0).containsExactlyInAnyOrder(*Face.BLANK.opposables)
        }

        @Test
        @DisplayName("should return the symbols on abs(face % dice.getNumberOfFaces) when given a face beyond limits")
        fun getSymbolsOfFaces_returnsSymbols_givenOutOfRange() {
            assertThat(dice.getSymbolsOnFace(7)).hasSize(1).containsExactlyInAnyOrder(*face1.opposables)
            assertThat(dice.getSymbolsOnFace(-3)).hasSize(0).containsExactlyInAnyOrder(*Face.BLANK.opposables)
            assertThat(dice.getSymbolsOnFace(-44)).hasSize(2).containsExactlyInAnyOrder(*face2.opposables)
        }
    }

    @Nested
    @DisplayName("getFaces")
    internal inner class GetFaces {
        @Test
        @DisplayName("should return a Map which reflects the order of array given in constructor ")
        fun getFaces_returnsMap() {
            val dice = Dice("test", Face.BLANK, faceGa)
            val result = dice.faces
            assertThat(result)
                    .containsEntry(0, Face.BLANK)
                    .containsEntry(1, faceGa)
        }

        @Test
        @DisplayName("should return a Map which reflects the order of array given in constructor ")
        fun getFaces_returnsEmptyMap() {
            val dice = Dice.BLANK
            val result = dice.faces
            assertThat(result).isEmpty()
        }
    }

    @Nested
    @DisplayName("equals")
    internal inner class Cequals {

        private val sameId = Dice("id")
        private val otherDice = Dice("other_id")
        private val origin = Dice("id", Face.BLANK, Face.BLANK)

        private val testArgs = mapOf(
                origin to true,
                sameId to true,
                otherDice to false,
                Dice.BLANK to false,
                Object() to false,
                null to false)

        @TestFactory
        @DisplayName("should return false when comparing to NOT AbstractSymbol")
        fun equals_returns_givenOtherInstance() = testArgs.map {
            DynamicTest.dynamicTest("should return ${it.value} when given ${it.key}") {
                assertThat(origin == it.key).isEqualTo(it.value)
            }
        }
    }
}
