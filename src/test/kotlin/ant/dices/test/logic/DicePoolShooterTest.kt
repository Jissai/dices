package ant.dices.test.logic

import ant.dices.logic.DicePoolShooter
import ant.dices.logic.Randomizer
import ant.dices.model.Dice
import ant.dices.model.DicePool
import ant.dices.model.Symbol
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

@ExtendWith(MockKExtension::class)
internal class DicePoolShooterTest {

    @Nested
    @DisplayName("Shoot")
    internal inner class Shoot {

        @RelaxedMockK
        private lateinit var randomizer: Randomizer

        @RelaxedMockK
        private lateinit var dice: Dice

        @RelaxedMockK
        private lateinit var pool: DicePool

        private lateinit var shooter: DicePoolShooter

        private val symbol1 = Symbol("id", "opp")
        private val symbol2 = Symbol("id", "opp")

        @BeforeEach
        fun setup() {
            every { randomizer.nextFace(any()) } returns 1
            every { dice.getSymbolsOnFace(1) } returns listOf(symbol1, symbol2)
            shooter = DicePoolShooter(randomizer)
        }

        @ParameterizedTest(name = "should return {0} * 2 Symbols when shooting {0} dice")
        @ValueSource(ints = [1, 2, 10])
        fun returns_symbol_givenDice(nbDice: Int) {
            every { pool.list() } returns mapOf(dice to nbDice)
            val symbols = shooter.shoot(pool)
            assertThat(symbols.size).isEqualTo(nbDice * 2) // as per mock initialization, 2 symbols per faces
        }
    }
}
