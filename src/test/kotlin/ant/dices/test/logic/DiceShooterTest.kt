package ant.dices.test.logic

import ant.dices.logic.DiceShooter
import ant.dices.logic.Randomizer
import ant.dices.model.Dice
import ant.dices.model.Face
import ant.dices.model.Symbol
import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class DiceShooterTest {

    @Nested
    @DisplayName("Shoot")
    internal inner class Shoot {

        private val symbol1 = Symbol("id", "opp")
        private val symbol2 = Symbol("opp", "id")
        private val randomizer = mockk<Randomizer>()
        private val shooter = DiceShooter(randomizer)
        private val dice: Dice = Dice("test", Face(symbol1, symbol2))

        init {
            every { randomizer.nextFace(any()) } returns 1
        }

        @Test
        @DisplayName("should return a list of symbols on the resulting face")
        fun returns_symbol_givenDice() {
            val result = shooter.shoot(dice)
            assertThat(result.size).isEqualTo(2)
            assertThat(result).containsExactlyInAnyOrder(symbol2, symbol1)
        }
    }
}