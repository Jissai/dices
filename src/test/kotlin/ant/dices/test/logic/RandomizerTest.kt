package ant.dices.test.logic

import ant.dices.logic.Randomizer
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.within
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

internal class RandomizerTest {

    val repetitions: Int = 10000
    val randomizer = Randomizer()

    @ParameterizedTest(name = "D{0} should be equiprobable")
    @ValueSource(ints = [1, 2, 4, 6, 8, 10, 12, 20])
    fun next_face(numberOfFaces: Int) {
        val spread = launch(repetitions) {
            randomizer.nextFace(numberOfFaces)
        }
        println("$spread")

        (0 until numberOfFaces).forEach {
            assertThat(spread.frequency(it)).isCloseTo(1f / numberOfFaces, within(0.1f))
        }
    }

    private fun launch(repetition: Int, what: () -> Int): Spread = Spread(repetition, (0 until repetition)
            .fold(mutableListOf<Int>(), { aggregator, _ ->
                aggregator.add(what())
                aggregator

            })
            .groupingBy { it }
            .eachCount())


    internal class Spread(private val total: Int, m: Map<Int, Int>) : HashMap<Int, Int>(m) {
        fun frequency(key: Int): Float = when {
            this[key] == null -> 0f
            else -> this[key]!!.toFloat().div(this.total)
        }
    }

}

