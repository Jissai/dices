package ant.dices.test.dal

import ant.dices.dal.DbAccess
import ant.dices.dal.SymbolDao
import ant.dices.model.CriticalSymbol
import ant.dices.model.Symbol
import ant.dices.model.inf.IOpposable
import io.jsondb.JsonDBException
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class SymbolDaoTest {

    lateinit var dao: SymbolDao
    var symbol: IOpposable = Symbol("id", "opposite")
    var equivalence: IOpposable = Symbol("opposite", "id")
    var critical: IOpposable = CriticalSymbol("crit-id", "crit-opposite", equivalence)

    @BeforeEach
    fun setUp() {
        dao = SymbolDao(DbAccess.test())
    }

    @AfterEach
    fun tearDown() {
        dao.dropDb()
    }

    private fun createDataSet() {
        dao.upsert(symbol)
        dao.upsert(critical)
    }

    @Nested
    @DisplayName("Upsert")
    inner class Upsert {

        @Test
        @DisplayName("should write non-existing symbol in DB then read it without loss")
        internal fun create_insert_Symbol_in_db() {
            val symbol = Symbol("id", "opposite")
            dao.upsert(symbol)
            val result = dao.read("id")
            assertThat(result).isEqualToComparingFieldByField(symbol)
        }

        @Test
        @DisplayName("should overwrite existing symbol in DB then read it without loss")
        internal fun create_double_insert_Symbol_in_db() {
            val symbol = Symbol("id", "opposite")
            dao.upsert(symbol)
            dao.upsert(symbol)
            val result = dao.read("id")
            assertThat(result).isEqualToComparingFieldByField(symbol)
        }

        @Test
        @DisplayName("should write non-existing CriticalSymbol in DB then read it without loss")
        internal fun create_insert_CriticalSymbol_in_db() {
            val equivalence = Symbol("id", "opposite")
            val critical = CriticalSymbol("crit-id", "crit-opposite", equivalence)
            dao.upsert(critical)
            val result = dao.read("crit-id")
            assertThat(result).isEqualToComparingFieldByFieldRecursively(critical)
        }
    }

    @Nested
    @DisplayName("List")
    inner class List {
        @BeforeEach
        internal fun setUp() {
            createDataSet()
        }

        @Test
        @DisplayName("should list every Symbol in DB")
        internal fun list_retrieve_every_element_from_db() {
            val results = dao.list()
            assertThat(results).hasSize(3)
        }

        @Test
        @DisplayName("should return empty array if no symbol in DB ")
        internal fun list_retrieve_empty_array_from_db() {
            dao.dropDb()
            dao.createDb()
            val results = dao.list()
            assertThat(results).hasSize(0)
        }
    }

    @Nested
    @DisplayName("Remove")
    inner class Remove {
        @BeforeEach
        internal fun setUp() {
            createDataSet()
        }

        @Test
        @DisplayName("should remove existing symbol in DB")
        internal fun remove_existing_symbol_in_db() {
            assertThat(dao.delete("id")).isEqualTo(true)
            val results = dao.list()
            assertThat(results).hasSize(2).containsExactlyInAnyOrder(equivalence, critical)
        }

        @Test
        @DisplayName("should notify when trying to remove non existing symbol in DB")
        internal fun remove_non_existing_symbol_in_db() {
            assertThat(dao.delete("id")).isEqualTo(true)
            assertThat(dao.delete("id")).isEqualTo(false)
            val results = dao.list()
            assertThat(results).hasSize(2).containsExactlyInAnyOrder(equivalence, critical)
        }

        @Test
        @DisplayName("should remove existing CriticalSymbol in DB. Leave references")
        internal fun remove_existing_CriticalSymbol_in_db() {
            assertThat(dao.delete("crit-id")).isEqualTo(true)
            val results = dao.list()
            assertThat(results).hasSize(2).containsExactlyInAnyOrder(symbol, equivalence)
        }

        @Test
        @DisplayName("should throw if trying to delete referenced Symbol in DB. Leave references")
        internal fun remove_existing_referenced_Symbol_in_db() {
            val thrown = catchThrowable { dao.delete("opposite") }
            assertThat(thrown)
                    .isInstanceOf(JsonDBException::class.java)
                    .hasMessageMatching("^Deletion of id.*It is still referenced by id.*$")
            val results = dao.list()
            assertThat(results).hasSize(3).containsExactlyInAnyOrder(symbol, equivalence, critical)
        }
    }
}
