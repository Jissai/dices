package ant.dices.test.dal

import ant.dices.dal.DbAccess
import ant.dices.dal.DiceDao
import ant.dices.dal.SymbolDao
import ant.dices.model.CriticalSymbol
import ant.dices.model.Dice
import ant.dices.model.Face
import ant.dices.model.Symbol
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class DiceDaoTest {
    lateinit var dao: DiceDao
    lateinit var symbolDao: SymbolDao

    val club = Symbol("club", "diamond")
    val diamond = Symbol("diamond", "club")
    val spade = CriticalSymbol("spade", "heart", club)
    val heart = CriticalSymbol("heart", "spade", diamond)
    val dice1 = Dice("dice_card_2", Face(club, heart), Face(diamond, spade))
    val dice2 = Dice("dice_card_6", Face(club), Face(heart), Face(diamond), Face(spade))

    @BeforeEach
    fun setUp() {
        val jsonDbTemplate = DbAccess.test()
        symbolDao = SymbolDao(jsonDbTemplate)
        dao = DiceDao(jsonDbTemplate, symbolDao)
    }

    @AfterEach
    fun tearDown() {
        symbolDao.dropDb()
        dao.dropDb()
    }

    @Nested
    @DisplayName("Upsert")
    inner class Upsert {

        @Test
        @DisplayName("should write BLANK dice in DB then read it without loss")
        internal fun create_insert_Blank_Dice_in_db() {
            dao.upsert(Dice.BLANK)
            val result = dao.read(Dice.BLANK.name)
            assertThat(result).isEqualToComparingFieldByFieldRecursively(Dice.BLANK)
        }

        @Test
        @DisplayName("should write non-existing dice in DB then read it without loss")
        internal fun create_insert_Dice_in_db() {
            dao.upsert(dice1)
            val result = dao.read(dice1.name)
            assertThat(result).isEqualToComparingFieldByFieldRecursively(dice1)
        }

        @Test
        @DisplayName("should overwrite existing dice in DB then read it without loss")
        internal fun create_double_insert_Dice_in_db() {
            dao.upsert(dice1)
            dao.upsert(dice1)
            val result = dao.read(dice1.name)
            assertThat(result).isEqualToComparingFieldByFieldRecursively(dice1)
        }
    }

    @Nested
    @DisplayName("List")
    inner class List {
        @BeforeEach
        internal fun setUp() {
            createDataSet()
        }

        @Test
        @DisplayName("should list every Dice in DB")
        internal fun list_retrieve_every_element_from_db() {
            val results = dao.list()
            assertThat(results).hasSize(3)
        }

        @Test
        @DisplayName("should return empty array if no dices in DB ")
        internal fun list_retrieve_empty_array_from_db() {
            dao.dropDb()
            dao.createDb()
            val results = dao.list()
            assertThat(results).hasSize(0)
        }
    }

    @Nested
    @DisplayName("Remove")
    inner class Remove {
        @BeforeEach
        internal fun setUp() {
            createDataSet()
        }

        @Test
        @DisplayName("should remove existing dice in DB")
        internal fun remove_existing_dice_in_db() {
            assertThat(dao.delete(Dice.BLANK.name)).isEqualTo(true)
            val results = dao.list()
            assertThat(results).hasSize(2).containsExactlyInAnyOrder(dice1, dice2)
        }

        @Test
        @DisplayName("\"should notify when trying to remove non existing dice in DB")
        internal fun remove_existing_referenced_Symbol_in_db() {
            assertThat(dao.delete(Dice.BLANK.name)).isEqualTo(true)
            assertThat(dao.delete(Dice.BLANK.name)).isEqualTo(false)
            val results = dao.list()
            assertThat(results).hasSize(2).containsExactlyInAnyOrder(dice1, dice2)
        }
    }

    private fun createDataSet() {
        dao.upsert(Dice.BLANK)
        dao.upsert(dice1)
        dao.upsert(dice2)
    }
}