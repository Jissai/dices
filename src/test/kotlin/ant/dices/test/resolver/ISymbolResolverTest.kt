package ant.dices.test.resolver

import ant.dices.model.CriticalSymbol
import ant.dices.model.Symbol
import ant.dices.model.inf.IOpposable
import ant.dices.resolver.SymbolResolverStringList
import ant.dices.resolver.SymbolResolverSymbolLong
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.TestFactory

internal class ISymbolResolverTest {
    var resolvers = listOf(SymbolResolverStringList(), SymbolResolverSymbolLong())

    @Nested
    @DisplayName("resolve")
    internal inner class Resolve {

        private var ga = Symbol("ga", "zo")
        private var bu = Symbol("bu")
        private var zo = Symbol("zo", "ga")
        private var meu = Symbol("meu")
        private var sha = CriticalSymbol("sha", "dock", ga)
        private var dock = CriticalSymbol("dock", "sha", zo)

        @TestFactory
        fun resolve_givenSame_returnsTwo() = resolvers.map {
            dynamicTest("${it.javaClass.simpleName} should return unmodified list when given 2 additive symbols") {
                val symbols = listOf<IOpposable>(ga, bu)
                val result = it.resolve(symbols)
                assertThat(result).hasSize(2)
            }
        }

        @TestFactory
        fun resolve_givenOpposite_returnsEmpty() = resolvers.map {
            dynamicTest("${it.javaClass.simpleName} should return an empty list when given 2 opposite symbols") {
                val symbols = listOf<IOpposable>(ga, zo)
                val result = it.resolve(symbols)
                assertThat(result).hasSize(0)
            }
        }

        @TestFactory
        fun resolve_givenOpposite_returnsRemain() = resolvers.map {
            dynamicTest("${it.javaClass.simpleName} should return the resulting element(s) when opposition occurs") {
                val symbols = listOf<IOpposable>(ga, zo, meu)
                val result = it.resolve(symbols)
                assertThat(result).hasSize(1)
                assertThat(result).containsEntry("meu", 1)
            }
        }

        @TestFactory
        fun resolve_givenManyOpposite_returnsRemain() = resolvers.map {
            dynamicTest("${it.javaClass.simpleName} should delete only one opposite for an occurrence") {
                val symbols = listOf<IOpposable>(ga, zo, meu, zo)
                val result = it.resolve(symbols)
                assertThat(result).hasSize(2)
                assertThat(result).containsEntry("meu", 1)
                assertThat(result).containsEntry("zo", 1)
            }
        }

        @TestFactory
        fun resolve_notEnoughOpposite_returnsRemain() = resolvers.map {
            dynamicTest("${it.javaClass.simpleName} should delete only one occurrence if not enough opposite") {
                val symbols = listOf<IOpposable>(ga, zo, ga, meu, zo, ga)
                val result = it.resolve(symbols)
                assertThat(result).hasSize(2)
                assertThat(result).containsEntry("meu", 1)
                assertThat(result).containsEntry("ga", 1)
            }
        }

        @TestFactory
        fun resolve_givenCritical_addSymbols() = resolvers.map {
            dynamicTest("${it.javaClass.simpleName} should list the critical and add relevant Symbols to the result when dealing with Criticals") {
                val symbols = listOf<IOpposable>(sha)
                val result = it.resolve(symbols)
                assertThat(result).hasSize(2)
                assertThat(result).containsEntry("sha", 1)
                assertThat(result).containsEntry("ga", 1)
            }
        }

        @TestFactory
        fun resolve_given_critical_remove_opposite_equivalence() = resolvers.map {
            dynamicTest("${it.javaClass.simpleName}   should list the critical and cancel opposite equivalence Symbols when present") {
                val symbols = listOf<IOpposable>(sha, zo)
                val result = it.resolve(symbols)
                assertThat(result).hasSize(1)
                assertThat(result).containsEntry("sha", 1)
            }
        }

        @TestFactory
        fun resolve_givena_critical_remove_opposite_critical() = resolvers.map {
            dynamicTest("${it.javaClass.simpleName}  should cancel opposite Criticals (and equivalences)") {
                val symbols = listOf<IOpposable>(sha, dock)
                val result = it.resolve(symbols)
                assertThat(result).hasSize(0)
            }
        }
    }
}
