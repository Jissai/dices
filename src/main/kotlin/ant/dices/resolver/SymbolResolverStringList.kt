package ant.dices.resolver

import ant.dices.model.inf.IOpposable
import ant.dices.resolver.inf.AbstractResolver
import org.jetbrains.annotations.NotNull
import java.util.*

class SymbolResolverStringList: AbstractResolver() {

    override fun internalResolve(@NotNull symbols: Collection<IOpposable>): Map<String, Int> {

        // group every symbols by id to a Queue of Symbols
        val groupedSymbols: Map<String, LinkedList<IOpposable>> = symbols
                .groupBy { it.id }
                .mapValues { LinkedList(it.value) }

        // for every symbol, if we find an opposite symbol in the map we remove both of them from the queues
        groupedSymbols.forEach { group ->
            val symbolsIterator = group.value.iterator()
            while (symbolsIterator.hasNext()) {
                val symbol = symbolsIterator.next()
                val oppositeSymbolList = groupedSymbols[symbol.opposite]
                if (oppositeSymbolList?.size ?: 0 > 0) {
                    oppositeSymbolList!!.remove()
                    symbolsIterator.remove()
                }
            }
        }

        // return a Map with the number of remaining Symbols
        return groupedSymbols
                .filterValues { !it.isEmpty() }
                .mapValues { it.value.size }
    }
}
