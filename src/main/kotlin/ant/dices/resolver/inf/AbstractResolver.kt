package ant.dices.resolver.inf

import ant.dices.model.inf.IOpposable
import ant.dices.model.inf.IOpposableEquivalence
import org.jetbrains.annotations.NotNull

abstract class AbstractResolver: ISymbolResolver {

    override fun resolve(@NotNull symbols: Collection<IOpposable>): Map<String, Int> {
        return internalResolve(enrichListWithCriticalEquivalence(symbols))
    }

    abstract fun internalResolve(@NotNull symbols: Collection<IOpposable>): Map<String, Int>

    private fun enrichListWithCriticalEquivalence(symbols: Collection<IOpposable>): Collection<IOpposable> {
        return symbols.fold(mutableListOf()) { aggregator, it ->
            aggregator.add(it)
            if (it is IOpposableEquivalence) aggregator.addAll(it.equivalence)
            aggregator
        }
    }
}