package ant.dices.resolver.inf

import ant.dices.model.inf.IOpposable

interface ISymbolResolver {

    /**
     * Given a list of Symbols, try to find matching pairs of opposite Symbols and remove them from the final result.
     *
     * @param symbols unordered list of Symbols to resolve
     * @return a summary map of each symbol ID paired with their respective number of occurrence in the list
     */
    fun resolve(symbols: Collection<IOpposable>): Map<String, Int>
}

