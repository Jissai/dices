package ant.dices.resolver

import ant.dices.model.Symbol
import ant.dices.model.inf.IOpposable
import ant.dices.resolver.inf.AbstractResolver
import org.jetbrains.annotations.NotNull
import kotlin.math.min

class SymbolResolverSymbolLong: AbstractResolver() {

    override fun internalResolve(@NotNull symbols: Collection<IOpposable>): Map<String, Int> {

        val symbolToOccurrences: MutableMap<IOpposable, Int> = groupBySymbolToOccurrences(symbols)

        // for each symbol if we find an opposite symbol in the list we remove both symbols
        symbolToOccurrences.forEach { (key, occurrences) ->
            if (occurrences > 0) {
                val target = Symbol(key.opposite)
                val oppositeOccurrences = symbolToOccurrences[target] ?: 0
                if (oppositeOccurrences > 0) {
                    val toSubtract = min(occurrences, oppositeOccurrences)
                    symbolToOccurrences.replace(target, oppositeOccurrences - toSubtract)
                    symbolToOccurrences.replace(key, occurrences - toSubtract)
                }
            }
        }

        return filterOutEmptyRecords(symbolToOccurrences)
    }

    private fun groupBySymbolToOccurrences(criticalEnrichedSymbols: Collection<IOpposable>): MutableMap<IOpposable, Int> {
        return criticalEnrichedSymbols
                .groupingBy { it }
                .eachCountTo(mutableMapOf())
    }

    private fun filterOutEmptyRecords(groupedSymbols: Map<IOpposable, Int>): Map<String, Int> {
        return groupedSymbols
                .filterValues { it != 0 }
                .mapKeys { it.key.id }
    }
}

