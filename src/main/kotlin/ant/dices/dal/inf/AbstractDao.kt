package ant.dices.dal.inf

import io.jsondb.InvalidJsonDbApiUsageException
import io.jsondb.JsonDBException
import io.jsondb.JsonDBTemplate

abstract class AbstractDao<SCHEMA, MODEL>(
        private val jsonDBTemplate: JsonDBTemplate,
        protected val type: Class<out SCHEMA>) {

    init {
        this.createDb()
    }

    /**
     * Destroy the collection used to store [SCHEMA].
     */
    open fun dropDb() {
        this.jsonDBTemplate.dropCollection(type)
    }

    /**
     * Create an empty collection to store [SCHEMA].
     */
    open fun createDb() {
        try {
            this.jsonDBTemplate.createCollection(type)
        }
        catch (e: InvalidJsonDbApiUsageException) {
            // no operation
        }
    }

    /**
     * insert or update a [SCHEMA] into the collection in DB
     *
     * @param model the model to convert and insert/update
     */
    open fun upsert(model: MODEL) {
        this.jsonDBTemplate.upsert<Any>(toSchema(model))
    }

    /**
     * Remove a [SCHEMA] from the collection in DB.
     *
     * @param id the primary key of the symbol to remove.
     * @throws JsonDBException if the given id is still referenced by at least another Symbol.
     */
    @Throws(JsonDBException::class)
    open fun delete(id: String): Boolean {
        val result = this.jsonDBTemplate.findById(id, type)
        return when {
            result != null -> this.jsonDBTemplate.remove(result, type) != null
            else -> false
        }
    }

    open fun read(id: String): MODEL {
        val result = this.jsonDBTemplate.findById(id, type)
        return fromSchema(result)
    }

    open fun list(): Collection<MODEL> {
        val results = this.jsonDBTemplate.findAll(type)
        return results.map { result -> this.fromSchema(result) }
    }

    abstract fun fromSchema(input: SCHEMA): MODEL
    abstract fun toSchema(input: MODEL): SCHEMA
}