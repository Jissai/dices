package ant.dices.dal.schema

import io.jsondb.annotation.Document
import io.jsondb.annotation.Id

@Document(collection = "dices", schemaVersion = "1.0")
data class DiceDbSchema(
        @Id var name: String? = null,
        var faces: Map<Int, List<String>>? = hashMapOf()
)