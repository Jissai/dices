package ant.dices.dal.schema

import io.jsondb.annotation.Document
import io.jsondb.annotation.Id

@Document(collection = "symbols", schemaVersion = "1.0")
data class SymbolDbSchema(
        @Id var id: String? = null,
        var oppositeId: String? = null,
        var equivalenceIds: List<String> = listOf())
