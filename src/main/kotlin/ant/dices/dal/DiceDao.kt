package ant.dices.dal

import ant.dices.dal.inf.AbstractDao
import ant.dices.dal.schema.DiceDbSchema
import ant.dices.model.Dice
import ant.dices.model.Face
import ant.dices.model.inf.IOpposable
import io.jsondb.JsonDBTemplate

class DiceDao(jsonDBTemplate: JsonDBTemplate, private val symbolDao: SymbolDao)
    : AbstractDao<DiceDbSchema, Dice>(jsonDBTemplate, DiceDbSchema::class.java) {

    override fun upsert(model: Dice) {
        model.faces.forEach { face ->
            face.value.opposables.forEach { this.symbolDao.upsert(it) }
        }
        super.upsert(model)
    }

    override fun toSchema(input: Dice): DiceDbSchema {
        val mapFace = input.faces.mapValues { entry -> entry.value.opposables.map { it.id } }
        return DiceDbSchema(input.name, mapFace)
    }

    override fun fromSchema(input: DiceDbSchema): Dice {
        val mapFaces: Map<Int, Face> = input.faces!!.mapValues { entry: Map.Entry<Int, List<String>> ->
            val symbols: List<IOpposable> = entry.value.map {
                this.symbolDao.read(it)
            }
            Face(*symbols.toTypedArray())
        }
        return Dice(input.name!!, *mapFaces.values.toTypedArray())
    }
}