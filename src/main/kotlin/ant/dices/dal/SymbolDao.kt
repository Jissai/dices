package ant.dices.dal

import ant.dices.dal.inf.AbstractDao
import ant.dices.dal.schema.SymbolDbSchema
import ant.dices.model.CriticalSymbol
import ant.dices.model.Symbol
import ant.dices.model.inf.IOpposable
import ant.dices.model.inf.IOpposableEquivalence
import io.jsondb.JsonDBException
import io.jsondb.JsonDBTemplate

class SymbolDao(private val jsonDBTemplate: JsonDBTemplate)
    : AbstractDao<SymbolDbSchema, IOpposable>(jsonDBTemplate, SymbolDbSchema::class.java) {

    override fun upsert(model: IOpposable) {
        if (model is IOpposableEquivalence) {
            model.equivalence.forEach {
                this.upsert(it)
            }
        }
        this.jsonDBTemplate.upsert<Any>(toSchema(model))
    }

    @Throws(JsonDBException::class)
    override fun delete(id: String): Boolean {

        val references = this.jsonDBTemplate.findAll( this.type)
                .filter { sc -> sc.equivalenceIds.contains(id) }
                .take(1)

        if (references.isEmpty()) {
            val result = this.jsonDBTemplate.findById(id,  this.type)

            return when {
                result != null -> this.jsonDBTemplate.remove(result, this.type) != null
                else -> false
            }
        }
        else {
            throw JsonDBException(String.format("Deletion of id: %s is forbidden. It is still referenced by id: %s", id, references[0].id))
        }
    }

    override fun fromSchema(input: SymbolDbSchema): IOpposable {
        if (input.equivalenceIds.isNotEmpty()) {
            val listEquivalence = input.equivalenceIds.map { this.read(it) }

            return CriticalSymbol(
                    input.id!!,
                    input.oppositeId!!,
                    *listEquivalence.toTypedArray())
        }
        return Symbol(input.id!!, input.oppositeId!!)
    }

    override fun toSchema(input: IOpposable): SymbolDbSchema {
        val schema = SymbolDbSchema()
        with(schema) {
            id = input.id
            oppositeId = input.opposite
            equivalenceIds = when (input) {
                is IOpposableEquivalence -> input.equivalence.map { it.id }
                else -> listOf()
            }
        }
        return schema
    }
}
