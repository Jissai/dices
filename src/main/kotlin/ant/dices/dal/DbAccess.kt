package ant.dices.dal

import io.jsondb.JsonDBTemplate
import java.nio.file.Paths

object DbAccess {

    internal var BASE_SCAN_PACKAGE = "ant.dices.dal.schema"

    fun test(): JsonDBTemplate {
        val userHome = System.getProperty("user.home")
        val db = Paths.get(userHome, "test")
        return JsonDBTemplate(db.toString(), BASE_SCAN_PACKAGE)
    }

    fun instance(): JsonDBTemplate {
        val userHome = System.getProperty("user.home")
        val db = Paths.get(userHome, "prod")
        return JsonDBTemplate(db.toString(), BASE_SCAN_PACKAGE)
    }
}
