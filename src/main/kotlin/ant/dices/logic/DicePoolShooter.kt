package ant.dices.logic

import ant.dices.model.DicePool
import ant.dices.model.inf.IOpposable

class DicePoolShooter(randomizer: Randomizer = Randomizer()) {

    private val diceShooter: DiceShooter = DiceShooter(randomizer)

    fun shoot(dicePool: DicePool): List<IOpposable> {
        return dicePool.list().map { entry ->
            (0 until entry.value).fold(mutableListOf<IOpposable>(), { agg, _ ->
                agg.addAll(diceShooter.shoot(entry.key))
                agg
            })
        }.flatten()
    }
}

