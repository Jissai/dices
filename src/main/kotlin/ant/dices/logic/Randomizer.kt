package ant.dices.logic

import kotlin.random.Random

class Randomizer {
    fun nextFace(numberOfFaces: Int): Int {
        return Random(Random.nextLong()).nextInt(numberOfFaces)
    }
}
