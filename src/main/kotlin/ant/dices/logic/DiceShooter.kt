package ant.dices.logic

import ant.dices.model.Dice
import ant.dices.model.inf.IOpposable

class DiceShooter(private val randomizer: Randomizer) {
    fun shoot(dice: Dice): Collection<IOpposable> = dice.getSymbolsOnFace(this.randomizer.nextFace(dice.numberOfFaces))
}