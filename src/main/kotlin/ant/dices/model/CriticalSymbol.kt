package ant.dices.model

import ant.dices.model.inf.AbstractSymbol
import ant.dices.model.inf.IOpposable
import ant.dices.model.inf.IOpposableEquivalence

class CriticalSymbol(id: String, opposite: String, vararg _equivalence: IOpposable)
    : AbstractSymbol(id, opposite), IOpposable, IOpposableEquivalence {

    override val equivalence: Collection<IOpposable>

    init {
        this.equivalence = listOf(*_equivalence)
    }
}
