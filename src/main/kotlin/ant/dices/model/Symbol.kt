package ant.dices.model

import ant.dices.model.inf.AbstractSymbol

class Symbol: AbstractSymbol {

    constructor(id: String): super(id, "")

    constructor(id: String, opposite: String): super(id, opposite)
}
