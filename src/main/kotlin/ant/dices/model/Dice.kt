package ant.dices.model

import ant.dices.model.inf.IOpposable
import kotlin.math.abs

class Dice(val name: String, vararg _faces: Face) {
    companion object {
        val BLANK: Dice = Dice("Blank")
    }

    val faces: Map<Int, Face>
    val numberOfFaces: Int
        get() = this.faces.size

    init {
        this.faces = _faces.mapIndexed { index, face -> index to face }.toMap()
    }

    fun getSymbolsOnFace(face: Int): Collection<IOpposable> {
        val actualFace: Int = abs(face) % this.numberOfFaces
        return listOf(*this.faces.getValue(actualFace).opposables)
    }

    override fun equals(other: Any?): Boolean {
        return when {
            (other == null) -> false
            (other !is Dice) -> false
            this === other -> true
            else -> name == other.name
        }
    }

    override fun hashCode(): Int = name.hashCode()
}
