package ant.dices.model.inf

open class AbstractSymbol protected constructor(override val id: String, override val opposite: String)
    : IOpposable {

    override fun equals(other: Any?): Boolean {
        return when {
            (other == null) -> false
            (other !is AbstractSymbol) -> false
            this === other -> true
            else -> id == other.id
        }
    }

    override fun hashCode(): Int = id.hashCode()
}
