package ant.dices.model.inf

interface IOpposable {
    val id: String
    val opposite: String
}
