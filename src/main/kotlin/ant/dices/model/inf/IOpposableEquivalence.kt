package ant.dices.model.inf

interface IOpposableEquivalence {
    val equivalence: Collection<IOpposable>
}
