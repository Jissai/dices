package ant.dices.model

class DicePool {

    private val dices: MutableMap<Dice, Int> = mutableMapOf()

    fun add(key: Dice, number: Int = 1): Int {
        val v = dices[key]
        return when {
            v != null -> dices.replace(key, v + number)!! + number
            else -> {
                dices[key] = number
                number
            }
        }
    }

    fun list() = this.dices.toMap()

    fun remove(key: Dice, number: Int = 1): Int {
        val v = dices[key]
        return when {
            v != null -> when {
                v <= number -> dices.remove(key)!! * 0
                else -> dices.replace(key, v - number)!! - number
            }
            else -> 0
        }
    }

}
