package ant.dices.model

import ant.dices.model.inf.IOpposable

class Face(vararg val opposables: IOpposable) {

    companion object {
        val BLANK = Face()
    }
}
